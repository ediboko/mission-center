/* ui/performance_page/gpu.blp
 *
 * Copyright 2023 Romeo Calota
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk 4.0;
using Adw 1;

template $PerformancePageGpu: Box {
  orientation: vertical;

  WindowHandle {
    child: Box description {
      orientation: vertical;
      margin-top: 10;
      spacing: 7;
      hexpand: true;

      Box {
        spacing: 20;

        Label gpu_id {
          styles [
            "title-1",
          ]

          hexpand: true;
          halign: start;
        }

        Label device_name {
          styles [
            "title-3",
          ]

          halign: end;
          ellipsize: middle;
        }
      }
    };
  }

  Box graph_top {
    orientation: vertical;

    margin-top: 10;

    Box {
      Label {
        styles [
          "caption",
        ]

        hexpand: true;
        halign: start;
        label: _("Overall utilization");
      }

      Label overall_percent {
        styles [
          "caption",
        ]
      }
    }

    $GraphWidget usage_graph_overall {
      vexpand: true;
      hexpand: true;

      height-request: 100;

      base-color: bind template.base-color;
      data-set-count: 1;
      scroll: true;
    }
  }

  Box decode_encode_graphs {
    orientation: horizontal;

    margin-top: 10;
    spacing: 10;

    Box encode_graph {
      orientation: vertical;

      Box {
        Label {
          styles [
            "caption",
          ]

          hexpand: true;
          halign: start;
          label: _("Video encode");
        }

        Label encode_percent {
          styles [
            "caption",
          ]
        }
      }

      $GraphWidget usage_graph_encode {
        vexpand: true;
        hexpand: true;

        height-request: 50;

        base-color: bind template.base-color;
        data-set-count: 1;
        scroll: true;
      }
    }

    Box decode_graph {
      orientation: vertical;

      Box {
        Label {
          styles [
            "caption",
          ]

          hexpand: true;
          halign: start;
          label: _("Video decode");
        }

        Label decode_percent {
          styles [
            "caption",
          ]
        }
      }

      $GraphWidget usage_graph_decode {
        vexpand: true;
        hexpand: true;

        height-request: 50;

        base-color: bind template.base-color;
        data-set-count: 1;
        scroll: true;
      }
    }
  }

  Box memory_graph {
    orientation: vertical;

    margin-top: 10;
    margin-bottom: 10;

    Box {
      Label {
        styles [
          "caption",
        ]

        hexpand: true;
        halign: start;
        label: _("Memory usage");
      }

      Label total_memory {
        styles [
          "caption",
        ]
      }
    }

    $GraphWidget usage_graph_memory {
      hexpand: true;

      height-request: 70;

      base-color: bind template.base-color;
      data-set-count: 1;
      scroll: true;
    }
  }

  Box details {
    spacing: 20;

    visible: bind template.summary-mode inverted;

    Box dynamic_data {
      spacing: 10;

      Adw.Clamp {
        orientation: horizontal;
        maximum-size: 250;

        child: Box left_column {
          orientation: vertical;
          spacing: 10;

          Box {
            orientation: vertical;
            spacing: 3;

            Label {
              styles [
                "caption",
              ]

              halign: start;
              label: _("Utilization");
            }

            Label utilization {
              styles [
                "title-4",
              ]

              halign: start;
            }
          }

          Box {
            orientation: vertical;
            spacing: 3;

            Label {

              styles [
                "caption",
              ]

              halign: start;
              label: _("Clock Speed");
            }

            Box {
              Label clock_speed_current {
                styles [
                  "title-4",
                ]

                ellipsize: middle;

                halign: start;
              }

              Label {
                styles [
                  "title-4",
                ]

                halign: start;
                label: " / ";
              }

              Label clock_speed_max {
                styles [
                  "title-4",
                ]

                ellipsize: middle;

                halign: start;
              }
            }
          }

          Box {
            orientation: vertical;
            spacing: 3;

            Label {
              styles [
                "caption",
              ]

              halign: start;
              label: _("Power draw");
            }

            Box {
              Label power_draw_current {
                styles [
                  "title-4",
                ]

                ellipsize: middle;

                halign: start;
              }

              Label {
                styles [
                  "title-4",
                ]

                halign: start;
                label: " / ";
              }

              Label power_draw_max {
                styles [
                  "title-4",
                ]

                ellipsize: middle;

                halign: start;
              }
            }
          }
        };
      }

      Adw.Clamp {
        orientation: horizontal;
        maximum-size: 250;

        child: Box right_column {
          orientation: vertical;
          spacing: 10;

          Box {
            orientation: vertical;
            spacing: 3;

            Label {
              styles [
                "caption",
              ]

              halign: start;
              label: _("Memory usage");
            }

            Box {
              Label memory_usage_current {
                styles [
                  "title-4",
                ]

                ellipsize: middle;

                halign: start;
              }

              Label {
                styles [
                  "title-4",
                ]

                halign: start;
                label: " / ";
              }

              Label memory_usage_max {
                styles [
                  "title-4",
                ]

                ellipsize: middle;

                halign: start;
              }
            }
          }

          Box {
            orientation: vertical;
            spacing: 3;

            Label {
              styles [
                "caption",
              ]

              halign: start;
              label: _("Memory speed");
            }

            Box {
              Label memory_speed_current {
                styles [
                  "title-4",
                ]

                ellipsize: middle;

                halign: start;
              }

              Label {
                styles [
                  "title-4",
                ]

                halign: start;
                label: " / ";
              }

              Label memory_speed_max {
                styles [
                  "title-4",
                ]

                ellipsize: middle;

                halign: start;
              }
            }
          }

          Box {
            orientation: vertical;
            spacing: 3;

            Label {
              styles [
                "caption",
              ]

              halign: start;
              label: _("Temperature");
            }

            Label temperature {
              styles [
                "title-4",
              ]

              ellipsize: middle;

              halign: start;
            }
          }
        };
      }
    }

    Box system_info {
      spacing: 10;

      Box labels {
        orientation: vertical;
        spacing: 3;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("OpenGL version:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Vulkan version:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("PCI Express speed:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("PCI bus address:");
        }
      }

      Box values {
        orientation: vertical;
        spacing: 3;

        Label opengl_version {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label vulkan_version {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label pcie_speed {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label pci_addr {
          styles [
            "caption",
          ]

          halign: start;
        }
      }
    }
  }

  PopoverMenu context_menu {
    has-arrow: false;
    menu-model: context_menu_model;
  }
}

menu context_menu_model {
  section {
    item {
      label: _("Graph _Summary View");
      action: "graph.summary";
    }

    submenu {
      label: _("_View");

      item {
        label: _("CP_U");
        action: "graph.cpu";
      }

      item {
        label: _("_Memory");
        action: "graph.memory";
      }

      item {
        label: _("_Disk");
        action: "graph.disk";
      }

      item {
        label: _("_Network");
        action: "graph.network";
      }

      item {
        label: _("_GPU");
        action: "graph.gpu";
      }
    }
  }

  section {
    item {
      label: _("_Copy");
      action: "graph.copy";
    }
  }
}
